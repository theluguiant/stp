from src.validators.json_schema.example_validator import TestRequest
from src.utils.general.time import timeStamp
import json

def app(event, context):
    try:
        data = {
            'pass': True 
        }
        return data
    except Exception as error:
        data = {
            'data': error.data if hasattr(error, 'data') else None,
            'statusCode': error.statusCode if hasattr(error, 'statusCode') else 500,
            'statusMessage': error.statusMessage if hasattr(error, 'statusMessage') else "Internal server error",
            'time': error.time if hasattr(error, 'time') else timeStamp(),
            'function': 'promotion_products',
            'pass': False
        }

        return data